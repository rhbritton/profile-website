;(function() {
    var throttle = function(type, name, obj) {
        obj = obj || window;
        var running = false;
        var func = function() {
            if (running) { return; }
            running = true;
            requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };

    /* init - you can init any event */
    throttle ("scroll", "optimizedScroll");
})()

;(function() {
	var body = document.getElementsByTagName('body')[0]
	  , header = document.getElementById('header')
	  , backToTop = document.getElementById('backToTop')
	  , backToTopText = document.querySelector('.backToTop span')

	// handle event
	window.addEventListener("optimizedScroll", function() {
		if(body.scrollTop > header.clientHeight) {
			backToTop.style.height = '3em'
			backToTop.style.width = '3em'
			backToTop.style.right = '1em'
			backToTop.style.bottom = '1em'
		} else {
			backToTop.style.height = '0'
			backToTop.style.width = '0'
			backToTop.style.right = '2.5em'
			backToTop.style.bottom = '2.5em'
		}
	})
})()