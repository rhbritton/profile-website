window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})()

;(function() {

	var parallaxNodes = document.querySelectorAll('[data-parallax]')

	;[].forEach.call(parallaxNodes, function(parallaxNode, i) {
		console.log('test')
		var img = parallaxNode.querySelector('img')
		console.log(img)


		// parallaxNodes[0].addEventListener('DOMAttrModified', function(e) {
		// 	console.log(1)
		//   if (e.attrName === 'style') {
		//     console.log('prevValue: ' + e.prevValue, 'newValue: ' + e.newValue);
		//   }
		// }, false);

	})


	parallaxEffect()

	window.onscroll = getScrollPosition

	function getScrollPosition() {
	    parallaxEffect()
	}

	function parallaxEffect() {
		;[].forEach.call(parallaxNodes, function(parallaxNode, i) {
			var img = parallaxNode.querySelector('img')

	    	var relativeTopOfElement = parallaxNode.getBoundingClientRect().top
	    	  , relativeBottomOfElement = relativeTopOfElement + parallaxNode.clientHeight

	    	if(relativeTopOfElement - document.body.clientHeight <= 0 && relativeBottomOfElement >= 0) {
	    		var calc = relativeTopOfElement - 0.5 * relativeTopOfElement
	    		if(parallaxNode.dataset.parallax == 'top') {
	    			img.style.top = -(calc)
	    			img.style.bottom = null
	    		} else {
	    			img.style.bottom = calc
	    		}
	    	}
		})

		requestAnimFrame(parallaxEffect)


	}


})()