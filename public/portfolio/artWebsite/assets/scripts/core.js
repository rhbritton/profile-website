;(function() {

	var gallery_images = document.querySelectorAll('[rel="photoswipe"]')
	  , items = []

	if(gallery_images.length) {
		[].forEach.call(gallery_images, function(image, i) {
			var img = image.querySelector('img')
			  , p = image.querySelector('p')
			  , new_img = new Image()

			new_img.onload = function() {
				items[i] = {
					  src: img.src
			        , w: img.naturalWidth
			        , h: img.naturalHeight
			        , title: p.innerHTML
				}

				if(image.addEventListener) {
					image.addEventListener("click", function(e) {
						e.preventDefault()
						viewGallery(image, i)
					})
				}
			}

			new_img.src = img.src;
		})
	}

	function viewGallery(image, i) {
		var gallery
		  , pswpElement

		pswpElement = document.querySelectorAll('.pswp')[0]

		options = {
			index: i
		}

		gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options)
		gallery.init()
	}

})()